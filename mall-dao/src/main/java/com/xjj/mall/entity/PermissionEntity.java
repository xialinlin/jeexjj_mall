/****************************************************
 * Description: Entity for t_mall_permission
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PermissionEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public PermissionEntity(){}
    private String name;//name
    private String permission;//permission
    /**
     * 返回name
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置name
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回permission
     * @return permission
     */
    public String getPermission() {
        return permission;
    }
    
    /**
     * 设置permission
     * @param permission permission
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.PermissionEntity").append("ID="+this.getId()).toString();
    }
}

