package com.xjj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application4core{

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application4core.class, args);
    }
}